import 'dart:io';
import 'dart:math';

void main() {
  stdout.write("Please input equation: ");
  String equation = stdin.readLineSync()!;
  var cal = calculator(equation);
  var result = cal.result;
  print("Result is: $result");
}

class calculator {
  List token = [];
  List postfix = [];
  num result = 0;

  calculator(String equation) {
    this.token = tokenizing(equation);
    this.postfix = infixToPostfix(token);
    this.result = evaluateToPostfix(postfix);
  }

  List tokenizing(String input) {
    List<String> list = [];
    String temp = "";
    for (var i = 0; i < input.length; i++) {
      if (input[i] == " ") {
        if (temp.isNotEmpty) {
          list.add(temp);
          temp = "";
        }
      } else {
        temp = temp + input[i];
      }
    }
    if (!input[input.length - 1].contains(" ")) {
      list.add(temp);
    }
    return list;
  }

  List infixToPostfix(List input) {
    List<String> listOperator = [];
    List<String> listPostfix = [];
    List<String> checkOperator = ["+", "-", "^", "*", "/" ,"%"]; //assign operator
    var operatorValues = {"+": 1, "-": 1, "^": 3, "*": 2, "/": 2 , "%":2}; //assign precedence operator
    for (var i = 0; i < input.length; i++) {
      //Operator check
      if (checkOperator.contains(input[i])) {
        while (listOperator.isNotEmpty &&
            listOperator[listOperator.length - 1] != "(" &&
            operatorValues[input[i]]! <=
                operatorValues[listOperator[listOperator.length - 1]]!) {
          listPostfix.add(listOperator[listOperator.length - 1]);
          listOperator.removeLast();
        }
        listOperator.add(input[i]);
      } else if (input[i] == "(" || input[i] == ")") {
        //Open parenthesis check
        if (input[i] == "(") {
          listOperator.add(input[i]);
        } //Close parenthesis check
        if (input[i] == ")") {
          while (listOperator[listOperator.length - 1] != "(") {
            listPostfix.add(listOperator[listOperator.length - 1]);
            listOperator.removeLast();
          }
          listOperator.removeLast();
        }
      } else {
        //Operand check
        if (double.parse(input[i]) is double) {
          listPostfix.add(input[i]);
        }
      }
    }

//pop values form listOperator
    while (listOperator.isNotEmpty) {
      listPostfix.add(listOperator[listOperator.length - 1]);
      listOperator.removeLast();
    }
    return listPostfix;
  }

//evaluate
  num evaluateToPostfix(List A) {
    List<num> values = [];
    var right;
    var left;
    var result;
    for (var a in A) {
      //Operand check
      if (!"+-*/^%".contains(a)) { //assign operator
        values.add(double.parse(a));
      } else {
        //assign the last value (pop) in right var.
        right = values[values.length - 1];
        //Remove an item from the end of values.
        values.removeLast();
        //assign the last value (pop) in left var.
        left = values[values.length - 1];
        //Remove an item from the end of values.
        values.removeLast();
        //Operator check and apply the operator to left and right.
        if (a == "+") {
          result = left + right;
          values.add(result);
        } else if (a == "-") {
          result = left - right;
          values.add(result);
        } else if (a == "*") {
          result = left * right;
          values.add(result);
        } else if (a == "/") {
          result = left / right;
          values.add(result);
        } else if (a == "^") {
          result = pow(left, right);
          values.add(result);
        } else if (a == "%") {
          result = left%right;
          values.add(result);
        }
      }
    }
    return values[0];
  }
}
